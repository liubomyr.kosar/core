USE [master]
GO
/****** Object:  Database [MyShopDB]    Script Date: 15.07.2016 7:41:01 ******/
CREATE DATABASE [MyShopDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MyShopDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\MyShopDB.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MyShopDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\MyShopDB_log.ldf' , SIZE = 1088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [MyShopDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MyShopDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MyShopDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MyShopDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MyShopDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MyShopDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MyShopDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [MyShopDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [MyShopDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MyShopDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MyShopDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MyShopDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MyShopDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MyShopDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MyShopDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MyShopDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MyShopDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [MyShopDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MyShopDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MyShopDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MyShopDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MyShopDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MyShopDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MyShopDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MyShopDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MyShopDB] SET  MULTI_USER 
GO
ALTER DATABASE [MyShopDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MyShopDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MyShopDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MyShopDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [MyShopDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [MyShopDB]
GO
/****** Object:  Table [dbo].[BaseProduct]    Script Date: 15.07.2016 7:41:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BaseProduct](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[SubcategoryId] [int] NOT NULL,
	[Manufacturer] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[DateOfManufacturing] [datetime] NOT NULL,
	[Rating] [int] NULL,
	[Price] [money] NOT NULL,
	[Discount] [money] NULL DEFAULT ((0.0)),
 CONSTRAINT [PK_BaseProduct] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 15.07.2016 7:41:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryDescription] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 15.07.2016 7:41:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TotalPrice] [money] NOT NULL,
	[DeliveryAddress] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 15.07.2016 7:41:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[OrderDetailId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
 CONSTRAINT [PK_OrderDetail] PRIMARY KEY CLUSTERED 
(
	[OrderDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Subcategory]    Script Date: 15.07.2016 7:41:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subcategory](
	[SubcategoryId] [int] IDENTITY(1,1) NOT NULL,
	[SubcategoryDescription] [nvarchar](300) NOT NULL,
	[CategoryId] [int] NOT NULL,
 CONSTRAINT [PK_Subcategory] PRIMARY KEY CLUSTERED 
(
	[SubcategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 15.07.2016 7:41:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[UserRole] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[BaseProduct] ON 

INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (5, N'Gigabyte PCI-Ex GeForce GTX 1080 8GB (GV-N1080D5X-8GD-B)', 2, N'Gigabyte', N'High end graphic card. Gigabyte PCI-Ex GeForce GTX 1080 Founders Edition 8GB GDDR5X (256bit) (1607/10000) (DVI, HDMI, 3 x DisplayPort) (GV-N1080D5X-8GD-B)', CAST(N'2016-01-02 00:00:00.000' AS DateTime), 5, 1000.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (6, N'MSI PCI-Ex GeForce GTX 960 4096MB (GTX 960 Gaming 4G)', 2, N'MSI', N'Middle end graphic card. MSI PCI-Ex GeForce GTX 960 4096MB GDDR5 (128bit) (1241/7010) (DVI, HDMI, 3 x DisplayPort) (GTX 960 Gaming 4G)', CAST(N'2015-12-15 00:00:00.000' AS DateTime), 4, 245.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (7, N'Intel Core i7-6700K', 1, N'Intel', N'High end desktop processor. Intel Core i7-6700K 4.0GHz/8GT/s/8MB (BX80662I76700K) s1151 BOX', CAST(N'2016-03-20 00:00:00.000' AS DateTime), 5, 400.0000, 5.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (8, N'Dell Alienware 17 R3 (A7S7161SDDW-46)', 5, N'Dell', N'High end gamer`s laptop. Dell Alienware 17 R3 (A7S7161SDDW-46)', CAST(N'2015-02-20 00:00:00.000' AS DateTime), 5, 2750.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (9, N'Novelty 120 Apple', 8, N'Novelty', N'Small sofa', CAST(N'2016-02-20 00:00:00.000' AS DateTime), 3, 205.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (10, N'GP Sofa Antares 3PL Just 01', 8, N'GP Sofa', N'Medium sofa', CAST(N'2016-05-20 00:00:00.000' AS DateTime), 5, 950.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (11, N'Kulik System Diamond Brown', 9, N'Kulik System', N'Office chair for chief', CAST(N'2014-10-07 00:00:00.000' AS DateTime), 5, 890.0000, 2.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (12, N'Office4You Conrad', 9, N'Office4You', N'Office chair (middle end)', CAST(N'2014-12-11 00:00:00.000' AS DateTime), 5, 175.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (13, N'LOWA Renegade GTX MID TF', 11, N'LOWA', N'Men`s shoes for mountains', CAST(N'2012-10-11 00:00:00.000' AS DateTime), 5, 235.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (14, N'Nike Air Max Invigor 749680-414 45', 11, N'Nike', N'Men`s sport shoes', CAST(N'2016-05-09 00:00:00.000' AS DateTime), 4, 160.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (15, N'Diesel T-Joe-Ba Maglietta 00SN5J/0CAKZ', 12, N'Diesel', N'Men`s t-shirt', CAST(N'2016-04-09 00:00:00.000' AS DateTime), 5, 93.0000, 10.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (16, N'Adidas S18254 COOL365 POLO', 12, N'Adidas', N'Men`s t-shirt', CAST(N'2016-01-09 00:00:00.000' AS DateTime), 4, 25.0000, 5.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (17, N'The North Face Men’s Sangro Jacket T0A3X5', 15, N'The North Face', N'Men`s autumn jacket', CAST(N'2015-01-09 00:00:00.000' AS DateTime), 5, 175.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (18, N'The North Face Men’s Venture Jacket T0A8AR', 15, N'The North Face', N'Men`s autumn jacket', CAST(N'2015-02-09 00:00:00.000' AS DateTime), 5, 170.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (19, N'The North Face Men’s Tethian Jacket T0CEC4 S V2R- Adder', 15, N'The North Face', N'Men`s spring jacket', CAST(N'2015-02-09 00:00:00.000' AS DateTime), 5, 242.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (20, N'Toptul 82', 19, N'Toptul', N'Personal toolbox', CAST(N'2010-02-21 00:00:00.000' AS DateTime), 5, 93.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (21, N'Mazda RX-8 R3', 20, N'Mazda', N'Sport car', CAST(N'2010-01-01 00:00:00.000' AS DateTime), 5, 14000.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (22, N'Lamborghini Gallardo LP560-4', 20, N'Lamborghini', N'Super car', CAST(N'2013-01-01 00:00:00.000' AS DateTime), 5, 170000.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (23, N'Dodge Viper SRT10', 20, N'Dodge', N'Sport car', CAST(N'2006-01-01 00:00:00.000' AS DateTime), 5, 48000.0000, 0.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (24, N'Suzuki GSX-R750', 21, N'Suzuki', N'Sportbike', CAST(N'2016-01-01 00:00:00.000' AS DateTime), 5, 13200.0000, 5.0000)
INSERT [dbo].[BaseProduct] ([ProductId], [Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount]) VALUES (25, N'Orbea MX 29 20 2016', 22, N'Orbea', N'Hardtale', CAST(N'2016-01-01 00:00:00.000' AS DateTime), 5, 1000.0000, 25.0000)
SET IDENTITY_INSERT [dbo].[BaseProduct] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (1, N'Personal Computers')
INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (2, N'Electronics')
INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (3, N'Furniture')
INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (4, N'Clothes')
INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (5, N'Tools')
INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (6, N'Vehicles')
INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (7, N'Personal Computers')
INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (8, N'Electronics')
INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (9, N'Furniture')
INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (10, N'Clothes')
INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (11, N'Tools')
INSERT [dbo].[Category] ([CategoryId], [CategoryDescription]) VALUES (12, N'Vehicles')
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([OrderId], [UserId], [TotalPrice], [DeliveryAddress]) VALUES (1, 1, 170000.0000, N'Brighton Beach, NY')
INSERT [dbo].[Order] ([OrderId], [UserId], [TotalPrice], [DeliveryAddress]) VALUES (2, 2, 360.0000, N'First Avenue, Detroit')
INSERT [dbo].[Order] ([OrderId], [UserId], [TotalPrice], [DeliveryAddress]) VALUES (3, 4, 1645.0000, N'Fifth Street, California')
INSERT [dbo].[Order] ([OrderId], [UserId], [TotalPrice], [DeliveryAddress]) VALUES (4, 1, 14200.0000, N'Brighton Beach, NY')
INSERT [dbo].[Order] ([OrderId], [UserId], [TotalPrice], [DeliveryAddress]) VALUES (5, 5, 61200.0000, N'Silent Hills, Denwer')
INSERT [dbo].[Order] ([OrderId], [UserId], [TotalPrice], [DeliveryAddress]) VALUES (6, 1, 80700.0000, N'Brighton Beach, NY')
SET IDENTITY_INSERT [dbo].[Order] OFF
SET IDENTITY_INSERT [dbo].[OrderDetail] ON 

INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (1, 1, 22)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (2, 2, 14)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (3, 2, 16)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (4, 2, 17)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (5, 3, 5)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (6, 3, 6)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (7, 4, 24)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (8, 3, 7)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (9, 4, 25)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (10, 5, 23)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (11, 5, 24)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (12, 6, 23)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (13, 6, 24)
INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId]) VALUES (14, 6, 25)
SET IDENTITY_INSERT [dbo].[OrderDetail] OFF
SET IDENTITY_INSERT [dbo].[Subcategory] ON 

INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (1, N'Processors', 1)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (2, N'Graphics Cards', 1)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (3, N'HDD-s', 1)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (4, N'SSD-s', 1)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (5, N'Laptops', 2)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (6, N'Tablets', 2)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (7, N'Cell phones', 2)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (8, N'Sofas', 3)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (9, N'Chairs', 3)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (10, N'Tables', 3)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (11, N'Shoes', 4)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (12, N'T-Shirts', 4)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (13, N'Trousers', 4)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (14, N'Shorts', 4)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (15, N'Jackets', 4)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (16, N'Screwdrivers', 5)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (17, N'Perforators', 5)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (18, N'Saws', 5)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (19, N'Toolkits', 5)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (20, N'Cars', 6)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (21, N'Motorcycles', 6)
INSERT [dbo].[Subcategory] ([SubcategoryId], [SubcategoryDescription], [CategoryId]) VALUES (22, N'Bicycles', 6)
SET IDENTITY_INSERT [dbo].[Subcategory] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserId], [Username], [Password], [FirstName], [LastName], [Email], [UserRole]) VALUES (1, N'JamesFord', N'JamesFord1970', N'James', N'Ford', N'james.ford@gmail.com', N'user')
INSERT [dbo].[User] ([UserId], [Username], [Password], [FirstName], [LastName], [Email], [UserRole]) VALUES (2, N'RyanRoss', N'RyanRoss1984', N'Ryan', N'Ross', N'ryan.ross@gmail.com', N'user')
INSERT [dbo].[User] ([UserId], [Username], [Password], [FirstName], [LastName], [Email], [UserRole]) VALUES (3, N'JohnBrown', N'JohnBrown1991', N'John', N'Brown', N'john.brown@gmail.com', N'admin')
INSERT [dbo].[User] ([UserId], [Username], [Password], [FirstName], [LastName], [Email], [UserRole]) VALUES (4, N'PeterMcCoy', N'PeterMcCoy1962', N'Peter', N'McCoy', N'peter.mccoy@gmail.com', N'user')
INSERT [dbo].[User] ([UserId], [Username], [Password], [FirstName], [LastName], [Email], [UserRole]) VALUES (5, N'JuliaStevens', N'JuliaStevens1988', N'Julia', N'Stevens', N'julia.stevens@gmail.com', N'user')
SET IDENTITY_INSERT [dbo].[User] OFF
ALTER TABLE [dbo].[BaseProduct]  WITH CHECK ADD  CONSTRAINT [FK_BaseProduct_Subcategory] FOREIGN KEY([SubcategoryId])
REFERENCES [dbo].[Subcategory] ([SubcategoryId])
GO
ALTER TABLE [dbo].[BaseProduct] CHECK CONSTRAINT [FK_BaseProduct_Subcategory]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_User]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_BaseProduct] FOREIGN KEY([ProductId])
REFERENCES [dbo].[BaseProduct] ([ProductId])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_BaseProduct]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([OrderId])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_Order]
GO
ALTER TABLE [dbo].[Subcategory]  WITH CHECK ADD  CONSTRAINT [FK_Subcategory_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[Subcategory] CHECK CONSTRAINT [FK_Subcategory_Category]
GO
ALTER TABLE [dbo].[BaseProduct]  WITH CHECK ADD CHECK  (([Rating]>=(1) AND [Rating]<=(5)))
GO
USE [master]
GO
ALTER DATABASE [MyShopDB] SET  READ_WRITE 
GO
