use MyShopDB
go

insert into [User]
	([Username], [Password], [FirstName], [LastName], [Email], [UserRole])
values 
	('JamesFord', 'JamesFord1970', 'James', 'Ford', 'james.ford@gmail.com', 'user'),
	('RyanRoss', 'RyanRoss1984', 'Ryan', 'Ross', 'ryan.ross@gmail.com', 'user'),
	('JohnBrown', 'JohnBrown1991', 'John', 'Brown', 'john.brown@gmail.com', 'admin'),
	('PeterMcCoy', 'PeterMcCoy1962', 'Peter', 'McCoy', 'peter.mccoy@gmail.com', 'user'),
	('JuliaStevens', 'JuliaStevens1988', 'Julia', 'Stevens', 'julia.stevens@gmail.com', 'user')
go

insert into [Order]
	([UserId], [TotalPrice], [DeliveryAddress]) 
values 
	(1, 100500, 'Brighton Beach, NY'),
	(2, 1450, 'First Avenue, Detroit'),
	(4, 5000, 'Fifth Street, California'),
	(1, 20500, 'Brighton Beach, NY'),
	(5, 18000, 'Silent Hills, Denwer'),
	(1, 80700, 'Brighton Beach, NY')
go

insert into [Category]
	([CategoryDescription]) 
values 
	('Personal Computers'),	--1
	('Electronics'),
	('Furniture'),
	('Clothes'),
	('Tools'),				--5
	('Vehicles')
go

insert into [Subcategory]
	([SubcategoryDescription], [CategoryId])
values 
	('Processors', 1),			--1
	('Graphics Cards', 1),
	('HDD-s', 1),  
	('SSD-s', 1), 
	('Laptops', 2),
	('Tablets', 2),
	('Cell phones', 2),

	('Sofas', 3),				--8
	('Chairs', 3),
	('Tables', 3),

	('Shoes', 4),				--11
	('T-Shirts', 4),
	('Trousers', 4),
	('Shorts', 4),
	('Jackets', 4),

	('Screwdrivers', 5),		--16
	('Perforators', 5),
	('Saws', 5),
	('Toolkits', 5),

	('Cars', 6),				--20
	('Motorcycles', 6),
	('Bicycles', 6)
go

insert into [BaseProduct]
	([Title], [SubcategoryId], [Manufacturer], [Description], [DateOfManufacturing], [Rating], [Price], [Discount])	 
values 
	('Gigabyte PCI-Ex GeForce GTX 1080 8GB (GV-N1080D5X-8GD-B)', 2, 'Gigabyte', 
		'High end graphic card. Gigabyte PCI-Ex GeForce GTX 1080 Founders Edition 8GB GDDR5X (256bit) (1607/10000) (DVI, HDMI, 3 x DisplayPort) (GV-N1080D5X-8GD-B)',
		'2016-01-02', 5, 1000, 0),
	('MSI PCI-Ex GeForce GTX 960 4096MB (GTX 960 Gaming 4G)', 2, 'MSI', 
		'Middle end graphic card. MSI PCI-Ex GeForce GTX 960 4096MB GDDR5 (128bit) (1241/7010) (DVI, HDMI, 3 x DisplayPort) (GTX 960 Gaming 4G)',
		'2015-12-15', 4, 245, 0),
	('Intel Core i7-6700K', 1, 'Intel', 
		'High end desktop processor. Intel Core i7-6700K 4.0GHz/8GT/s/8MB (BX80662I76700K) s1151 BOX',
		'2016-03-20', 5, 400, 5),

	('Dell Alienware 17 R3 (A7S7161SDDW-46)', 5, 'Dell', 
		'High end gamer`s laptop. Dell Alienware 17 R3 (A7S7161SDDW-46)',
		'2015-02-20', 5, 2750, 0),


	('Novelty 120 Apple', 8, 'Novelty', 'Small sofa', '2016-02-20', 3, 205, 0),
	('GP Sofa Antares 3PL Just 01', 8, 'GP Sofa', 'Medium sofa', '2016-05-20', 5, 950, 0),

	('Kulik System Diamond Brown', 9, 'Kulik System', 'Office chair for chief', '2014-10-07', 5, 890, 2),
	('Office4You Conrad', 9, 'Office4You', 'Office chair (middle end)', '2014-12-11', 5, 175, 0),


	('LOWA Renegade GTX MID TF', 11, 'LOWA', 'Men`s shoes for mountains', '2012-10-11', 5, 235, 0),
	('Nike Air Max Invigor 749680-414 45', 11, 'Nike', 'Men`s sport shoes', '2016-05-09', 4, 160, 0),

	('Diesel T-Joe-Ba Maglietta 00SN5J/0CAKZ', 12, 'Diesel', 'Men`s t-shirt', '2016-04-09', 5, 93, 10),
	('Adidas S18254 COOL365 POLO', 12, 'Adidas', 'Men`s t-shirt', '2016-01-09', 4, 25, 5),

	('The North Face Men�s Sangro Jacket T0A3X5', 15, 'The North Face', 'Men`s autumn jacket', '2015-01-09', 5, 175, 0),
	('The North Face Men�s Venture Jacket T0A8AR', 15, 'The North Face', 'Men`s autumn jacket', '2015-02-09', 5, 170, 0),
	('The North Face Men�s Tethian Jacket T0CEC4 S V2R- Adder', 15, 'The North Face', 'Men`s spring jacket', '2015-02-09', 5, 242, 0),


	('Toptul 82', 19, 'Toptul', 'Personal toolbox', '2010-02-21', 5, 93, 0),


	('Mazda RX-8 R3', 20, 'Mazda', 'Sport car', '2010-01-01', 5, 14000, 0),
	('Lamborghini Gallardo LP560-4', 20, 'Lamborghini', 'Super car', '2013-01-01', 5, 170000, 0),
	('Dodge Viper SRT10', 20, 'Dodge', 'Sport car', '2006-01-01', 5, 48000, 0),

	('Suzuki GSX-R750', 21, 'Suzuki', 'Sportbike', '2016-01-01', 5, 13200, 5),


	('Orbea MX 29 20 2016', 22, 'Orbea', 'Hardtale', '2016-01-01', 5, 1000, 25)

go

insert into [OrderDetail]
	([OrderId], [ProductId])
values 
	(1, 22),	--1
	(2, 14),
	(2, 16),
	(2, 17),
	(3, 5),

	(3, 6),
	(4, 24),
	(3, 7),	
	(4, 25),
	(5, 23),
	(5, 24),
	(6, 23),
	(6, 24),
	(6, 25)

go